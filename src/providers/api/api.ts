import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppHelper } from "../../app/app.helper";

@Injectable()
export class ApiProvider {

  user_id: any = null;
  //private baseUrl = 'http://mahe.inmenteweb.com/pheco';
  private baseUrl = 'http://localhost:8100/pheco';

  constructor(public http: HttpClient) {
    this.user_id = (localStorage.getItem('user_id'));
  }

  setUser(id) {
    this.user_id = id;
    localStorage.setItem('user_id', this.user_id);
  }

  isLogin() {
    return this.user_id !== null;
  }

  post(endpoint, body, login = false) {

    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    return this.http.post(`${this.baseUrl}/${endpoint}`, AppHelper.parametize(body), {headers: headers})
      .toPromise()
  }

  get(endpoint, params = {}, login = false) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    return this.http.get(`${this.baseUrl}/${endpoint}?${AppHelper.parametize(params)}`, {headers: headers})
      .toPromise()
  }
}

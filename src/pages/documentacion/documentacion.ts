import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ContactosPage } from "../contactos/contactos";
import { MedidoresMetrogasPage } from "../medidores-metrogas/medidores-metrogas";
import { MedidoresEdesurPage } from "../medidores-edesur/medidores-edesur";
import { PlanosPage } from "../planos/planos";
import { ReglamentoPage } from "../reglamento/reglamento";

/**
 * Generated class for the DocumentacionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-documentacion',
  templateUrl: 'documentacion.html',
})
export class DocumentacionPage {

  pages: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {
    this.pages = [
      {title: 'Planos generales', component: PlanosPage},
      {title: 'Reglamento', component: ReglamentoPage},
      {title: 'Contactos', component: ContactosPage},
      {title: 'Medidores Edisur', component: MedidoresEdesurPage},
      {title: 'Medidores metrogas', component: MedidoresMetrogasPage},
    ]
  }

  goPage(component) {
    this.navCtrl.push(component);
  }

}

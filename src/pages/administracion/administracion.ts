import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";

/**
 * Generated class for the AdministracionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-administracion',
  templateUrl: 'administracion.html',
})
export class AdministracionPage {
  mantenimiento: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider) {
  }

  ionViewDidLoad() {
    this.api.get('mantenimiento')
      .then((value: any) => {
        this.mantenimiento = value;
      })
  }

}

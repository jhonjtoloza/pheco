import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MedidoresEdesurPage } from './medidores-edesur';

@NgModule({
  declarations: [
    MedidoresEdesurPage,
  ],
  imports: [
    IonicPageModule.forChild(MedidoresEdesurPage),
  ],
})
export class MedidoresEdesurPageModule {
}

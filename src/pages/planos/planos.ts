import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PlanosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-planos',
  templateUrl: 'planos.html',
})
export class PlanosPage {

  planos;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.planos = [
      {title: 'Arquitectura', desc: 'Planos generales del edificio', url: 'http://domain.com/pathtofile.pdf'},
      {title: 'Estructura', desc: 'Plano de ubicación de vigas y columnas', url: 'http://domain.com/pathtofile.pdf'},
      {
        title: 'Instalación electrica',
        desc: 'Plano completo de tendido eléctrico y sus tablas correspondiente',
        url: 'http://domain.com/pathtofile.pdf'
      },
      {
        title: 'Instalación sanitaria',
        desc: 'Plano completo del tendido de cañerias cloacales, pluviales, agua fria y caliente',
        url: 'http://domain.com/pathtofile.pdf'
      },
    ]
  }

}

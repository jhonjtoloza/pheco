import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { HomePage } from "../home/home";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  tab = 'login';

  user = {
    email: 'ventas@mahe.com.ar',
    clave: '321321'
  };

  new_user = {
    email: '',
    clave: ''
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiProvider,
    private alert: AlertController) {
  }

  ionViewDidLoad() {

  }

  login() {
    if (this.user.email.length && this.user.clave.length) {
      this.api.post('login', this.user)
        .then((value: any) => {
          if (value.id !== 0) {
            this.api.setUser(value.id);
            this.navCtrl.setRoot(HomePage);
          } else {
            this.alert.create({
              message: 'Email o clave incorrecta'
            }).present()
          }
        }).catch(err => {
        let msj: string = err.error['0'];
        if (!msj.length)
          msj = 'Sin conexión';
        this.alert.create({
          message: msj
        }).present()
      })
    }
  }

  register() {
    this.api.post('signin', this.user)
      .then((value: any) => {
        if (value.error != undefined) {
          this.alert.create({
            message: value.error
          }).present()
        }
      }).catch(err => {
      console.log(err)
    })
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MedidoresMetrogasPage } from './medidores-metrogas';

@NgModule({
  declarations: [
    MedidoresMetrogasPage,
  ],
  imports: [
    IonicPageModule.forChild(MedidoresMetrogasPage),
  ],
})
export class MedidoresMetrogasPageModule {
}

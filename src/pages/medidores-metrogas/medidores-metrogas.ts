import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MedidoresMetrogasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-medidores-metrogas',
  templateUrl: 'medidores-metrogas.html',
})
export class MedidoresMetrogasPage {

  data = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.data = [
      {name: 'UF 11', number: '009019552'},
      {name: 'UF 12', number: '009019552'},
      {name: 'UF 13', number: '009019552'},
      {name: 'UF 14', number: '009019552'},
      {name: 'UF 15', number: '009019552'},
      {name: 'UF 16', number: '009019552'},
      {name: 'UF 17', number: '009019552'},
      {name: 'UF 18', number: '009019552'},
      {name: 'UF 19', number: '009019552'},
      {name: 'UF 20', number: '009019552'},
      {name: 'UF 21', number: '009019552'},
      {name: 'UF 22', number: '009019552'},
    ]
  }

}

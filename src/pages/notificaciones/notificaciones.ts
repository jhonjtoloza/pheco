import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";

/**
 * Generated class for the NotificacionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notificaciones',
  templateUrl: 'notificaciones.html',
})
export class NotificacionesPage {

  notificaciones: any[];
  text: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public toast: ToastController,
    public load: LoadingController) {
  }

  ionViewDidLoad(){
    this.loadData()
  }

  loadData() {
    let load = this.load.create({
      content: 'Cargando ...'
    });
    load.present();
    this.api.get('getNotificaciones')
      .then((value: any) => {
        load.dismiss();
        this.notificaciones = value
      }).catch(_ => {
      load.dismiss();
      this.toast.create({
        message: 'Fallo la conexión'
      }).present()
    })
  }

  postNotificacion() {
    if (this.text.length > 10) {
      let load = this.load.create({
        content: 'Enviando ...'
      });
      load.present();
      this.api.post('setNotificacion', {
        usuario_id: this.api.user_id,
        texto: this.text
      }).then(_ => {
        load.dismissAll();
        this.loadData();
      }).catch(_ => {
        load.dismissAll();
        this.toast.create({
          message: 'Fallo la conexión'
        }).present()
      })
    } else {
      this.toast.create({
        message: 'Excriba un texto de al menos 10 caracteres',
        duration: 2000
      }).present()
    }
  }

}

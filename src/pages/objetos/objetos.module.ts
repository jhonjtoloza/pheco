import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ObjetosPage } from './objetos';
import { ComponentsModule } from "../../components/components.module";
import { DatePipe } from "@angular/common";

@NgModule({
  declarations: [
    ObjetosPage,
  ],
  imports: [
    IonicPageModule.forChild(ObjetosPage),
    ComponentsModule
  ],
  providers: [
    DatePipe
  ]
})
export class ObjetosPageModule {
}

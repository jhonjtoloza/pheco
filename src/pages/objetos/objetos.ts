import { Component, ViewChild } from '@angular/core';
import { AlertController, DateTime, IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { DatePipe } from "@angular/common";
import * as moment from "moment";

/**
 * Generated class for the ObjetosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-objetos',
  templateUrl: 'objetos.html',
})
export class ObjetosPage {

  objetos = [];
  itemExpandHeight = 235;
  hoy = new Date();
  fecha_day: Date;
  fecha_hour: Date;
  @ViewChild('fecha', {read: DateTime}) fecha;
  @ViewChild('fecha_hora', {read: DateTime}) fecha_hora;
  item = null;
  min;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public dateFormater: DatePipe,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController) {
    this.min = moment().format('YYYY-MM-DD');
    console.log(this.min);
  }

  ionViewDidLoad() {
    this.api.get('bibliotecaobjetos')
      .then((value: any) => {
        this.objetos = value;
      })
  }

  expandItem(item) {
    this.objetos.map((listItem) => {
      if (item == listItem) {
        listItem.expanded = !listItem.expanded;
        setTimeout(() => {
          listItem.expand = !listItem.expand
        }, 100)
      } else {
        listItem.expand = false;
        listItem.expanded = false;
      }
      return listItem;
    });
  }

  reservar(item) {
    this.item = item;
    this.fecha.open();
  }

  oncloseFecha() {
    this.fecha_hora.open();
  }

  complete() {
    let fecha = `${this.fecha_day} ${this.fecha_hour}`;
    this.reservarAhora(this.item, fecha);
  }

  reservarAhora(item, fecha?) {
    let load = this.loadingCtrl.create({
      content: 'Enviando ...'
    });
    load.present();
    if (!fecha)
      fecha = this.dateFormater.transform(this.hoy, 'yyyy-MM-dd HH:mm');
    this.api.post('reservarObjeto', {objeto_id: item.objeto_id, fecha: fecha, usuario_id: this.api.user_id})
      .then((value: any) => {
        load.dismiss();
        this.alertCtrl.create({
          title: value.mensaje,
          buttons: [
            {text: 'OK', role: 'success'}
          ]
        }).present()
      }).catch(err => {
      load.dismiss();
    })
  }

}

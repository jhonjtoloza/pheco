import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ObjetosPage } from "../objetos/objetos";
import { DocumentacionPage } from "../documentacion/documentacion";
import { CuentaPage } from "../cuenta/cuenta"
import { NotificacionesPage } from "../notificaciones/notificaciones";
import { AdministracionPage } from "../administracion/administracion";
import { SeguridadPage } from "../seguridad/seguridad";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pages: Array<{ title: string, component: any, icon: string }>;

  constructor(public navCtrl: NavController) {
    this.pages = [
      {title: 'Biblioteca de objetos', component: ObjetosPage, icon: 'construct'},
      {title: 'Seguridad', component: SeguridadPage, icon: 'videocam'},
      {title: 'Documentación', component: DocumentacionPage, icon: 'book'},
      {title: 'Administración', component: AdministracionPage, icon: 'settings'},
      {title: 'Notificaciones', component: NotificacionesPage, icon: 'chatboxes'},
      {title: 'Mi cuenta', component: CuentaPage, icon: 'contact'},
    ];
  }

  goPage(component) {
    this.navCtrl.push(component);
  }
}

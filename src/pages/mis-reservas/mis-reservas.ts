import { Component } from '@angular/core';
import {
  AlertController,
  IonicPage,
  LoadingController,
  NavController,
  NavParams,
  ToastController
} from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import * as moment from "moment";

/**
 * Generated class for the MisReservasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mis-reservas',
  templateUrl: 'mis-reservas.html',
})
export class MisReservasPage {
  reservas: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private load: LoadingController,
    private toast: ToastController,
    public api: ApiProvider,
    private alert: AlertController) {
  }

  loading;

  showLoad() {
    this.loading = this.load.create({
      content: 'Cargando ...'
    });
    this.loading.present();
  }

  hideLoad() {
    this.loading.dismissAll();
  }

  showToast(content) {
    this.toast.create({
      message: content,
      duration: 3000
    }).present()
  }

  ionViewDidLoad() {
    this.ini();
  }

  ini(){
    this.showLoad();
    this.api.post('reservasUsuario', {
      usuario_id: this.api.user_id
    },).then((value: any) => {
      this.reservas = value;
      this.hideLoad();
    }).catch(_ => {
      this.hideLoad();
      this.showToast('Sin conexión');
    })
  }

  isBefore(date){
    let now = moment();
    let dateCheck = moment(date);
    return dateCheck.isBefore(now);
  }

  cancelarReserva(id, index) {
    this.alert.create({
      message: 'Esta seguro que desea cancelar la reserva ?',
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
        },
        {
          text: 'SI',
          handler: () => {
            this.showLoad();
            this.api.post('cancelarReserva', {
              reserva_id: id
            }).then((value: any) => {
              this.reservas.splice(index, 1);
              this.hideLoad();
              this.showToast(value.mensaje)
            }).catch(_ => {
              this.hideLoad();
              this.showToast('Sin conexión');
            });
          }
        }
      ]
    }).present();
  }

}

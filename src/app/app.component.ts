import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from "../pages/login/login";
import { ApiProvider } from "../providers/api/api";
import { ObjetosPage } from "../pages/objetos/objetos";
import { DocumentacionPage } from "../pages/documentacion/documentacion";
import { SeguridadPage } from "../pages/seguridad/seguridad";
import { AdministracionPage } from "../pages/administracion/administracion";
import { NotificacionesPage } from "../pages/notificaciones/notificaciones";
import { CuentaPage } from "../pages/cuenta/cuenta";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{ title: string, component: any, icon: string }>;

  constructor(private api: ApiProvider, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    if (this.api.isLogin())
      this.rootPage = HomePage;
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      {title: 'Princial', component: HomePage, icon: 'home'},
      {title: 'Biblioteca de objetos', component: ObjetosPage, icon: 'construct'},
      {title: 'Seguridad', component: SeguridadPage, icon: 'videocam'},
      {title: 'Documentación', component: DocumentacionPage, icon: 'book'},
      {title: 'Administración', component: AdministracionPage, icon: 'settings'},
      {title: 'Notificaciones', component: NotificacionesPage, icon: 'chatboxes'},
      {title: 'Mi cuenta', component: CuentaPage, icon: 'contact'},
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.setPages([
      {page: HomePage},
      {page: page}
    ])
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiProvider } from '../providers/api/api';
import { LoginPageModule } from "../pages/login/login.module";
import { HttpClientModule } from "@angular/common/http";
import { ObjetosPageModule } from "../pages/objetos/objetos.module";
import { ComponentsModule } from "../components/components.module";
import { DatePipe } from "@angular/common";
import { DocumentacionPageModule } from "../pages/documentacion/documentacion.module";
import { ContactosPageModule } from "../pages/contactos/contactos.module";
import { CuentaPageModule } from "../pages/cuenta/cuenta.module";
import { MisReservasPageModule } from "../pages/mis-reservas/mis-reservas.module";
import { SeguridadPageModule } from "../pages/seguridad/seguridad.module";
import { NotificacionesPageModule } from "../pages/notificaciones/notificaciones.module";
import { AdministracionPageModule } from "../pages/administracion/administracion.module";
import { MedidoresEdesurPageModule } from "../pages/medidores-edesur/medidores-edesur.module";
import { MedidoresMetrogasPageModule } from "../pages/medidores-metrogas/medidores-metrogas.module";
import { ReglamentoPageModule } from "../pages/reglamento/reglamento.module";
import { PlanosPageModule } from "../pages/planos/planos.module";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      mode: 'ios'
    }),
    HttpClientModule,
    ComponentsModule,
    LoginPageModule,
    ObjetosPageModule,
    DocumentacionPageModule,
    ContactosPageModule,
    CuentaPageModule,
    MisReservasPageModule,
    SeguridadPageModule,
    NotificacionesPageModule,
    AdministracionPageModule,
    MedidoresEdesurPageModule,
    MedidoresMetrogasPageModule,
    ReglamentoPageModule,
    PlanosPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    DatePipe
  ]
})
export class AppModule {
}
